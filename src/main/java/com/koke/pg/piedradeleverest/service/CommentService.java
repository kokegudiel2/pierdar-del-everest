package com.koke.pg.piedradeleverest.service;

import com.koke.pg.piedradeleverest.dto.CommentsDto;
import com.koke.pg.piedradeleverest.exceptions.PostNotFoundException;
import com.koke.pg.piedradeleverest.mapper.CommentMapper;
import com.koke.pg.piedradeleverest.model.Comment;
import com.koke.pg.piedradeleverest.model.NotificationEmail;
import com.koke.pg.piedradeleverest.model.Post;
import com.koke.pg.piedradeleverest.model.User;
import com.koke.pg.piedradeleverest.repository.CommentRepository;
import com.koke.pg.piedradeleverest.repository.PostRepository;
import com.koke.pg.piedradeleverest.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class CommentService {
	private static final String POST_URL = "";
	private final PostRepository postRepository;
	private final UserRepository userRepository;
	private final AuthService authService;
	private final CommentMapper commentMapper;
	private final CommentRepository commentRepository;
	private final MailContentBuilder mailContentBuilder;
	private final MailService mailService;

	public void save(CommentsDto commentsDto) {
		Post post = postRepository.findById(commentsDto.getPostId())
				.orElseThrow(() -> new PostNotFoundException(commentsDto.getPostId().toString()));
		Comment comment = commentMapper.map(commentsDto, post, authService.getCurrentUser());
		commentRepository.save(comment);

		String message = mailContentBuilder
				.build(authService.getCurrentUser() + " hizo un comentario en tu publicación." + POST_URL);
		sendCommentNotification(message, post.getUser());
	}

	private void sendCommentNotification(String message, User user) {
		mailService.sendMail(
				new NotificationEmail(user.getUsername() + " Comentó tu publicación", user.getEmail(), message));
	}

	public List<CommentsDto> getAllCommentsForPost(Long postId) {
		Post post = postRepository.findById(postId).orElseThrow(() -> new PostNotFoundException(postId.toString()));
		return commentRepository.findByPost(post).stream().map(commentMapper::mapToDto).collect(toList());
	}

	public List<CommentsDto> getAllCommentsForUser(String userName) {
		User user = userRepository.findByUsername(userName).orElseThrow(() -> new UsernameNotFoundException(userName));
		return commentRepository.findAllByUser(user).stream().map(commentMapper::mapToDto).collect(toList());
	}
}
