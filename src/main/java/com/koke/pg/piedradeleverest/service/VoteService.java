package com.koke.pg.piedradeleverest.service;

import com.koke.pg.piedradeleverest.dto.VoteDto;
import com.koke.pg.piedradeleverest.exceptions.PiedraDelEverestException;
import com.koke.pg.piedradeleverest.exceptions.PostNotFoundException;
import com.koke.pg.piedradeleverest.model.Post;
import com.koke.pg.piedradeleverest.model.Vote;
import com.koke.pg.piedradeleverest.repository.PostRepository;
import com.koke.pg.piedradeleverest.repository.VoteRepository;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static com.koke.pg.piedradeleverest.model.VoteType.UPVOTE;

@Service
@AllArgsConstructor
public class VoteService {

	private final VoteRepository voteRepository;
	private final PostRepository postRepository;
	private final AuthService authService;

	@Transactional
	public void vote(VoteDto voteDto) {
		Post post = postRepository.findById(voteDto.getPostId())
				.orElseThrow(() -> new PostNotFoundException("No se contró publicación con ID - " + voteDto.getPostId()));
		Optional<Vote> voteByPostAndUser = voteRepository.findTopByPostAndUserOrderByVoteIdDesc(post,
				authService.getCurrentUser());
		if (voteByPostAndUser.isPresent() && voteByPostAndUser.get().getVoteType().equals(voteDto.getVoteType())) {
			throw new PiedraDelEverestException("You have already " + voteDto.getVoteType() + "'d for this post");
		}
		if (UPVOTE.equals(voteDto.getVoteType())) {
			post.setVoteCount(post.getVoteCount() + 1);
		} else {
			post.setVoteCount(post.getVoteCount() - 1);
		}
		voteRepository.save(mapToVote(voteDto, post));
		postRepository.save(post);
	}

	private Vote mapToVote(VoteDto voteDto, Post post) {
		return Vote.builder().voteType(voteDto.getVoteType()).post(post).user(authService.getCurrentUser()).build();
	}
}
