package com.koke.pg.piedradeleverest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;

import com.koke.pg.piedradeleverest.config.SwaggerConfiguration;

@SpringBootApplication
@EnableAsync
@Import(SwaggerConfiguration.class)
public class PiedraDelEverestApplication {

	public static void main(String[] args) {
		SpringApplication.run(PiedraDelEverestApplication.class, args);
	}

}
