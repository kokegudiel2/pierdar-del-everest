package com.koke.pg.piedradeleverest.dto;

import com.koke.pg.piedradeleverest.model.VoteType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VoteDto {
	
    private VoteType voteType;
    private Long postId;
}
