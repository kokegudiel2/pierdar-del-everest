package com.koke.pg.piedradeleverest.exceptions;

@SuppressWarnings("serial")
public class SubredditNotFoundException extends RuntimeException {
    public SubredditNotFoundException(String message) {
        super(message);
    }
}
