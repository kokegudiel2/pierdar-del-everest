package com.koke.pg.piedradeleverest.exceptions;

@SuppressWarnings("serial")
public class PiedraDelEverestException extends RuntimeException {
    public PiedraDelEverestException(String exMessage, Exception exception) {
        super(exMessage, exception);
    }

    public PiedraDelEverestException(String exMessage) {
        super(exMessage);
    }
}
