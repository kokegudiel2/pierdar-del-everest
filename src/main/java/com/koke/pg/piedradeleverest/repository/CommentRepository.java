package com.koke.pg.piedradeleverest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.koke.pg.piedradeleverest.model.Comment;
import com.koke.pg.piedradeleverest.model.Post;
import com.koke.pg.piedradeleverest.model.User;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
	List<Comment> findByPost(Post post);

	List<Comment> findAllByUser(User user);
}
