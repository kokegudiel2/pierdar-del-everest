package com.koke.pg.piedradeleverest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.koke.pg.piedradeleverest.model.Post;
import com.koke.pg.piedradeleverest.model.User;
import com.koke.pg.piedradeleverest.model.Vote;

import java.util.Optional;

@Repository
public interface VoteRepository extends JpaRepository<Vote, Long> {
    Optional<Vote> findTopByPostAndUserOrderByVoteIdDesc(Post post, User currentUser);
}
