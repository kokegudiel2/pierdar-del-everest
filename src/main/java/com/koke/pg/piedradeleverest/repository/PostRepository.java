package com.koke.pg.piedradeleverest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.koke.pg.piedradeleverest.model.Post;
import com.koke.pg.piedradeleverest.model.Subreddit;
import com.koke.pg.piedradeleverest.model.User;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {
	List<Post> findAllBySubreddit(Subreddit subreddit);

	List<Post> findByUser(User user);
}
